package com.afdhal.todolistapp.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.afdhal.todolistapp.TaskModel

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    companion object {
        private val DB_NAME = "todolist2.db"
        private val DB_VERSION = 1
        private val TBL_TASK = "tblTask"
        private val ID = "id"
        private val TASK_NAME = "taskname"
        private val STATUS = "status"
    }

    override fun onCreate(p0: SQLiteDatabase?) {
//        val CREATE_TABLE = "DROP TABLE IF EXISTS $TBL_TASK"
        val CREATE_TABLE = ("CREATE TABLE $TBL_TASK ($ID INTEGER PRIMARY KEY, $TASK_NAME TEXT, $STATUS INTEGER);")
        p0?.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        val DROP_TABLE = "DROP TABLE IF EXISTS $TBL_TASK"
        p0!!.execSQL(DROP_TABLE)
        onCreate(p0)
    }

    fun getAllTask(): ArrayList<TaskModel> {
        val tasklist = ArrayList<TaskModel>()
        val selectQuery = "SELECT * FROM $TBL_TASK"
        val db = this.readableDatabase

        val cursor: Cursor?

        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: Exception) {
            e.printStackTrace()
            db.execSQL(selectQuery)
            return ArrayList()
        }
            if (cursor.moveToFirst()) {
                do {
                    val tasks = TaskModel()
                    tasks.id = Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(ID)))
                    tasks.name = cursor.getString(cursor.getColumnIndexOrThrow(TASK_NAME))
                    tasks.status = cursor.getInt(cursor.getColumnIndexOrThrow(STATUS))
                    tasklist.add(tasks)
                } while (cursor.moveToNext())   //fetch next data
            }
        return tasklist
    }

    //insert
    fun insertTask(tasks: TaskModel): Boolean {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(TASK_NAME, tasks.name)
        values.put(STATUS, tasks.status)
        val _success = db.insert(TBL_TASK, null, values)
        db.close()
        return (Integer.parseInt("$_success") != -1)        //if data is inserted then return true, else return false
    }

    //delete
    fun deleteTask(_id: Int): Boolean {
        val db: SQLiteDatabase = writableDatabase
        val _success = db.delete(TBL_TASK, ID + "=?", arrayOf(_id.toString())).toLong()
        return (Integer.parseInt("$_success") != -1)        //if data is deleted then return true, else return false
    }

    //delete completedtask
    fun deleteCompletedTask(_status: Int): Boolean {
        val db: SQLiteDatabase = writableDatabase
        val _success = db.delete(TBL_TASK, STATUS + "=?", arrayOf(_status.toString())).toLong()
        return (Integer.parseInt("$_success") != -1)        //if data is deleted then return true, else return false
    }

    //update
    fun updateTask(tasks: TaskModel): Boolean {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(TASK_NAME, tasks.name)
        values.put(STATUS, tasks.status)
        val _success =
            db.update(TBL_TASK, values, ID + "=?", arrayOf(tasks.id.toString())).toLong()
        db.close()
        return (Integer.parseInt("$_success") != -1)        //if data is updated then return true, else return false
    }
}