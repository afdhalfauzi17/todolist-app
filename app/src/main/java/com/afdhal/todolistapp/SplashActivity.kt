package com.afdhal.todolistapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        //Show splash screen for 1500ms
        Handler().postDelayed(
            {
                //Launch the main activity
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            },1500
        )
    }
}