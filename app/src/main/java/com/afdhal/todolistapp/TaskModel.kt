package com.afdhal.todolistapp

data class TaskModel(
    var id: Int = 0,
    var name: String = "",  // Name of the task
    var status: Int = 0     // 0=task not completed, 1=taskcompleted
){

}