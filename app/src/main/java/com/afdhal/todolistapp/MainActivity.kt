package com.afdhal.todolistapp

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.afdhal.todolistapp.database.DatabaseHelper
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private lateinit var btn_remove_completed: ImageButton
    private lateinit var btn_add_task: FloatingActionButton
    private lateinit var swipe_refresh : SwipeRefreshLayout
    private lateinit var rv_task_list: RecyclerView
    private lateinit var ed_task: EditText
    private lateinit var dbHelper: DatabaseHelper
    private lateinit var tv_no_task: TextView

    private var adapter: TaskAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dbHelper = DatabaseHelper(this)
        initView()
        getTask()

        btn_add_task.setOnClickListener { showAddDialog() }
        btn_remove_completed.setOnClickListener { deleteCompletedTask() }
        swipe_refresh.setOnRefreshListener { getTask()
            swipe_refresh.isRefreshing = false
        }

    }

    fun showAddDialog() {
        //inflate dialog
        val builder = AlertDialog.Builder(this)
        val view = LayoutInflater.from(this).inflate(R.layout.add_list, null)
        ed_task = view.findViewById(R.id.ed_task)

        builder.setView(view)
        builder.setTitle("Add Task")

        builder.setPositiveButton("Add") { _, _ -> addTask() }
        builder.setNegativeButton("Cancel") { _, _ -> }

        val alert = builder.create()
        alert.show()
    }

    fun getTask() {
        val taskList = dbHelper.getAllTask()
        adapter?.addItems(taskList)

        //show and hide "No Task to Show"
        if (taskList.size < 1){
            tv_no_task.visibility = View.VISIBLE
        } else{
            tv_no_task.visibility = View.INVISIBLE
        }
    }

    fun addTask() {
        val taskName = ed_task.text.toString()
        val taskStatus = 0  // 0 mean the task is not completed yet

        if (taskName.isEmpty()) {
            Toast.makeText(this, "Please fill the task name", Toast.LENGTH_SHORT).show()
        } else {
            val task = TaskModel(name = taskName, status = taskStatus)
            val success = dbHelper.insertTask(task)

            if (success) {
                getTask()
                Toast.makeText(this, "Task Added!", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task not saved!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    //show confirm dialog before remove the completed task
    fun deleteCompletedTask(){
        MaterialAlertDialogBuilder(this)
            .setTitle("Confirm")
            .setIcon(R.drawable.ic_baseline_warning_24)
            .setMessage("Remove completed task?")
            .setNegativeButton("cancel") {_, _->
                //do nothing
            }
            .setPositiveButton("remove"){_, _->
                val status = 1
                val success = dbHelper.deleteCompletedTask(status)  //delete task where status == 1

                if (success) {
                    getTask()
                    Toast.makeText(this, "Completed Task removed!", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "failed!", Toast.LENGTH_SHORT).show()
                }
            }
            .show()
    }

    fun initView() {
        btn_add_task = findViewById(R.id.btn_add_task)
        rv_task_list = findViewById(R.id.rv_task_list)
        btn_remove_completed = findViewById((R.id.btn_remove_completed))
        tv_no_task = findViewById(R.id.tv_no_task)
        swipe_refresh = findViewById(R.id.swipe_refresh)
        rv_task_list.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        adapter = TaskAdapter()
        rv_task_list.adapter = adapter
    }
}