package com.afdhal.todolistapp

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.afdhal.todolistapp.database.DatabaseHelper
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.util.*
import kotlin.collections.ArrayList

class TaskAdapter : RecyclerView.Adapter<TaskAdapter.TaskViewHolder>() {
    private var tasklist: ArrayList<TaskModel> = ArrayList()

    fun addItems(items: ArrayList<TaskModel>) {
        Collections.reverse(items)
        this.tasklist = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TaskViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_tasks, parent, false), tasklist
    )

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        val currentTask = tasklist[position]
        holder.bindView(currentTask)
        holder.tasklist = tasklist      //pass the updated tasklist into TaskViewHolder
    }

    override fun getItemCount(): Int {
        return tasklist.size
    }


    open class TaskViewHolder(var view: View, var tasklist: ArrayList<TaskModel>) :
        RecyclerView.ViewHolder(view), View.OnLongClickListener {

        val view2 = LayoutInflater.from(view.context).inflate(R.layout.add_list, null)
        val db = DatabaseHelper(view.context)
        var ed_task = view2.findViewById<EditText>(R.id.ed_task)
        var name = view.findViewById<TextView>(R.id.tv_task_item)
        var checkbox = view.findViewById<CheckBox>(R.id.checkbox_item)

        init {
            itemView.setOnLongClickListener(this)
        }

        fun bindView(task: TaskModel) {
            name.text = task.name
            if (task.status == 1) {
                checkbox.isChecked = true
                name.paintFlags = name.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                name.setTextColor(Color.LTGRAY)
            } else {
                checkbox.isChecked = false
                name.paintFlags = name.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                name.setTextColor(-1979711488)  //default textcolor
            }
            checkbox.setOnCheckedChangeListener { _, checked ->
                //STRIKE_THRU and change the color of completed task (toggle by checkbox)
                if (checked) {
                    name.paintFlags = name.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    name.setTextColor(Color.LTGRAY)
                    task.status = 1
                    db.updateTask(task)
                } else {
                    name.paintFlags = name.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                    name.setTextColor(-1979711488)  //defaultColor
                    task.status = 0
                    db.updateTask(task)
                }
            }
        }

        override fun onLongClick(p0: View?): Boolean {
            val inflater: LayoutInflater = LayoutInflater.from(view.context)
            val view = inflater.inflate(R.layout.add_list, null)

            val builder = AlertDialog.Builder(view.context)
                .setTitle("Action")
                .setView(view)

            ed_task = view.findViewById(R.id.ed_task)
            ed_task.setText(tasklist[adapterPosition].name)
                builder.setNegativeButton("update") { _, _ ->
                    confirmDialog("update")
                }
                .setPositiveButton("remove") { _, _ ->
                    confirmDialog("delete")
                }
                .setNeutralButton("cancel") { _, _ ->

                }

            builder.create()
            builder.show()

            return true
        }

        fun deleteItem(item: TaskModel) {
            val success = db.deleteTask(item.id)
            if (success) {
                Toast.makeText(view.context, "Task deleted!", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(view.context, "Delete failed", Toast.LENGTH_SHORT).show()
            }
            (view.context as MainActivity).getTask()    //call getTask function from MainActivity
        }

        fun updateItem(item: TaskModel) {
            item.name = ed_task.text.toString()

            if (item.name == "") return Toast.makeText(
                view.context,
                "Please fill the task name",
                Toast.LENGTH_SHORT
            ).show()

            val success = db.updateTask(item)
            if (success) {
                Toast.makeText(view.context, "Task updated!", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(view.context, "Update task failed!", Toast.LENGTH_SHORT).show()
            }
            (view.context as MainActivity).getTask()    //call getTask function from MainActivity
        }

        fun confirmDialog(mode: String) {
            MaterialAlertDialogBuilder(view.context)
                .setTitle("Confirm")
                .setIcon(R.drawable.ic_baseline_warning_24)
                .setMessage("Continue the action?")
                .setNegativeButton("cancel") { _, _ ->

                }
                .setPositiveButton(mode) { _, _ ->
                    if(mode=="update") {updateItem(tasklist[adapterPosition])}
                    else if (mode=="delete") {deleteItem(tasklist[adapterPosition])}
                }
                .show()
        }
    }
}